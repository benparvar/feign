package com.benparvar.feign.controller;

import com.benparvar.feign.client.UserClient;
import com.benparvar.feign.controller.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/user")
public class UserController {

    private final UserClient userClient;

    public UserController(UserClient userClient) {
        this.userClient = userClient;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<User> getUsers() {
        return userClient.getUsers();
    }
}
