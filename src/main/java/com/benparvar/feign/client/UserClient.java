package com.benparvar.feign.client;

import com.benparvar.feign.controller.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(name = "users", url = "https://jsonplaceholder.typicode.com")
public interface UserClient {

    @RequestMapping("/users")
    List<User> getUsers();
}
